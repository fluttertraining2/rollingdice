// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'dart:math';

final randomiz = Random();

class rollingDice extends StatefulWidget {
  const rollingDice({super.key});

  @override
  State<rollingDice> createState() => _rollingDiceState();
}

class _rollingDiceState extends State<rollingDice> {
  var currentRolled = 2;

  void rollDice() {
    setState(() {
      currentRolled = randomiz.nextInt(6) + 1;
      print(currentRolled);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Image.asset(
          'assets/images/dice-images/dice-$currentRolled.png',
          width: 200,
        ),
        const SizedBox(
          height: 50,
        ),
        OutlinedButton(
          onPressed: rollDice,
          style: TextButton.styleFrom(
            // padding: EdgeInsets.all(15),
            foregroundColor: Colors.white,
            textStyle: const TextStyle(
              fontSize: 28,
            ),
          ),
          child: const Text('Roll Dice'),
        )
      ],
    );
  }
}
