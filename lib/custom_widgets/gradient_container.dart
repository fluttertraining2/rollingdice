import 'package:flutter/material.dart';
import 'package:toggle_poc/custom_widgets/rolling_dice.dart';

class GradientContainer extends StatelessWidget {
  GradientContainer({super.key});

  @override
  Widget build(context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Color.fromARGB(255, 9, 95, 55),
            Color.fromARGB(255, 13, 117, 69),
            Color.fromARGB(255, 23, 139, 85),
          ],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        ),
      ),
      child: const Center(
        child: rollingDice(),
      ),
    );
  }
}
